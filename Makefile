# Писали на семинаре
TARGET=main
FLAGS=-felf64
LD=ld
SOURCE=$(wildcard *asm)
OBJ=$(SOURCE:.asm=.o)

.PHONY: all clean

all: $(TARGET)

%.o: %.asm
	nasm $(FLAGS) $< -o $@

$(TARGET): $(OBJ)
	$(LD) $^ -o $@

clean:
	rm -fr *.o main # не уверен что мэйн нужно удалять, но ладно
