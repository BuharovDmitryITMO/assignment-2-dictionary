global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
section .text
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .cycle: 
        cmp byte[rdi + rax], 0; проверяем не конец-ли строки 
        je .breakpoint      ; иначе выходим
        inc rax             ; считаем длину строки         
        jmp .cycle          ; повторяем цикл
    .breakpoint:
        ret                 ; возвращаем значение аккумулятора

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length      ; помещаем в аккумулятор длинну строки
    pop rsi                 ; помещаем ссылку на начало строки в rsi
    mov rdx, rax            ; помещаем длину строки в rdx
    mov rax, 1              ; write
    mov rdi, 1              ; stdout
    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                ; положить код в стек
    mov rax, 1              ; write
    mov rdi, 1              ; stdout
    mov rdx, 1              ; помещаем длину строки в rcx
    mov rsi, rsp            ; ссылка на символ в стеке
    syscall
    pop rdi                 ; удалить символ из стека
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`           ; кладем код новой строки
    jmp print_char         

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    mov rax, rdi            ; в rax записываем число для деления
    mov r9, 0               ; количество цифр
    mov r10, 10             ; записываем 10  в r10 для деления на 10
    push 0                  ; записываем конец строки

    .cycle:
        xor rdx, rdx        ; обнуляем rdx (для div)
        div r10             ; делим число на 10
        add rdx, '0'        ; переводим в ascii
        bswap rdx
        push rdx            ; записываем в стек
        add rsp, 7          ; сдвигаем стек
        inc r9              ; увеличиваем счетчик
        cmp rax, 0          ; если число закончилось -> выходим из цикла
        je .print_num
        jmp .cycle
    
    .print_num:
        mov rdi, rsp        ; адрес начала строки
        push r9             ; сохраняем счетчик
        call print_string   ; принтим цифру
        pop r9              ; возвращаем счетчик
        add rsp, r9 - 1     ; сдвигаем стек
        pop rdi
        ret 

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0              ; сравниваем число, меньше ли 0
    jl .negative            ; меньше 0 -> выводим минус
    jmp print_uint          ; иначе печатаем число

    .negative:
        push rdi
        mov rdi, '-'
        call print_char     ; печатаем '-'
        pop rdi
        neg rdi             ; отрицаем число и печатаем его
        jmp print_uint 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1               ; записываем 1 (считаем что строки равны)

    .cycle:
        mov r11b, byte[rsi] ; загружаем символ 2ой строки(тк память нельзя сравнивать с памятью)
        cmp byte[rdi], r11b ; сравниваем два символа    
        jne .not_equals     ; если не равны -> меняем rax
        cmp byte[rdi], 0    ; если равны и равны 0, то возвращаем 1
        je .breakpoint

        inc rdi             ; указатель 1ой строки + 1
        inc rsi             ; указатель 2ой строки + 1
        jmp .cycle

    .not_equals:
        mov rax, 0           ; rax = 0

    .breakpoint:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax            ; read
    xor rdi, rdi            ; in
    push 0                  ; пустой буфер для символа
    mov rsi, rsp            ; передаем адрес буфера
    mov rdx, 1              ; читаем 1 символ
    syscall
    pop rax                 ; запишем прочтенный символ в акум
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor r9, r9              ; счетчик для записи
    push r12                ; пушим регистр (callee-saved)
    push r13                ; пушим регистр (callee-saved)

    mov r12, rdi
    mov r13, rsi
    .cycle:
        call read_char      ; читаем символ строки

        cmp rax, ' '        ; проверяем на пробел
        je .is_first        ; если да, то первый ли?

        cmp rax, `\t`       ; проверяем на таб
        je .is_first        ; если да, то первый ли?

        cmp rax, `\n`       ; проверяем на энтер
        je .is_first        ; если да, то первый ли?

        cmp rax, 0          ; проверяем на конец строки
        je .success         ; если да, то мы прочли слово

        jmp .try_to_write    ; иначе это просто символ

    .is_first:
        cmp r9, 0           ; пробельные символы и мы еще не читали слово?
        je .cycle           ; тогда скипаем
        jmp .success        ; иначе мы прочли слово
    
    .try_to_write:
        cmp r13, r9         ; смотрим в буфер
        je .error           ; если он забит, то вернуть еррор
        mov [r12 + r9], rax ; иначе записать символ
        inc r9
        jmp .cycle      

    .success:
        mov rax, 0          ; дописываем 0 терминатор
        cmp r13, r9         ; смотрим в буфер
        je .error           ; если он забит, то вернуть еррор
        mov [r12 + r9], rax ; иначе записать символ
        mov rdx, r9         ; помещаем в rdx длину слова
        mov rax, r12        ; помещаем в rax адрес буфера
        jmp .ret

    .error:
        xor rax, rax        ; очищаем регистр при ошибке
        xor rdx, rdx        ; очищаем регистр при ошибке
    
    .ret:
        pop r13             ; возвращаем регистр (callee-saved)
        pop r12             ; возвращаем регистр (callee-saved)
        ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r9, r9                      ; счетчик длины числа
    mov r10, 10                     ; множитель для числа
    .cycle:
        xor r11, r11                ; temp для младших битов в rdi
        mov r11b, byte[rdi + r9]    ; запишем младшие биты из rdi + r9

        cmp r11, '0'                ; проверяем больше ли 0 код символа
        jl .breakpoint              ; если нет - то выходим (число кончилось)
        cmp r11, '9'                ; проверяем меньше ли 9 код символа
        jg .breakpoint              ; если нет - то выходим (число кончилось)
        cmp r11, 0                  ; проверяем закончилась ли строка
        je .breakpoint              ; если да - то выходим (число кончилось)

        sub r11b, '0'               ; вычитаем 48 (из ascii в число)
        mul r10                     ; умножаем результат на 10
        add rax, r11                ; прибавляем к результату новую цифру
        inc r9                      ; продолжаем итерироваться по числу

        jmp .cycle
    
    .breakpoint:
        cmp r9, 0                   ; если счетчик пустой
        je .error                   ; то числа не было
        mov rdx, r9                 ; иначе вернем число и длину
        ret

    .error:
        xor rax, rax                ; числа не было, возвращаем 0
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '+'  ; проверяем на + в начале
    je .signed          ; если + -> сравним как знаковое
    cmp byte[rdi], '-'  ; проверяем на + в начале
    je .signed          ; если - -> сравним как знаковое
    jmp parse_uint      ; если знака нет, то читаем число

    .signed:
        inc rdi         ; пропускаем знак
        call parse_uint ; читаем число
        inc rdx         ; учтем знак
        cmp byte[rdi - 1], '-'; если отрицательное
        neg rax         ; то отрицаем
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length  ; длина строки
    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx        ; проверяем длину слова и буфера
    jg .error           ; если слово длинее - ошибка

    xor r10, r10        ; счетчик длины строки
    .cycle:
        xor rax, rax    ; обнулим акум
        mov al, byte[rdi + r10]; запишем символ в акум
        mov byte[rsi + r10], al; запишем акум в буфер

        cmp rax, 0
        je .breakpoint
        inc r10         ; увеличим длину строки
        jmp .cycle      ; продолжим итерацию

    .error:
        xor rax, rax
        ret
    .breakpoint:
        mov rax, r10
        ret