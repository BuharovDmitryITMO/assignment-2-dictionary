section .bss
buffer: resb 256            ; Буффер на 256 символов

section .data
%include "words.inc"
%include "dict.inc"
%include "lib.inc"

greeting: db "Введите ключ: ", 0
success: db "Значение: ", 0
fail: db "Такой ключ отсутствует", 10, 0
error: db "Ошибка чтения буффер переполнен", 10, 0

section .text

global _start
_start:
    mov rdi, greeting       ; Загружаем приветствие 
    call print_string       ; Выводим его на экран
    mov rdi, buffer         ; Указываем ссылку на буффер
    mov rsi, 256            ; Указываем длину буффера
    call read_word          ; Читаем слово
    cmp rax, 0              ; Если не удалось прочитать
    je .reading_error;      ; Выводим ошибку
    mov rdi, rax            ; Указываем ключ
    mov rsi, NEXT_NODE      ; Указываем начало словаря
    call find_word          ; Пытаемся найти слово
    cmp rax, 0              ; Если не найдено
    je .not_found           ; Выводим ошибку
    mov rdi, rax            ; Rax в rdi
    add rdi, 8              ; Получаем ссылку на ключ
    push rdi                
    call string_length
    pop rdi
    lea rdi, [rdi + rax + 1]
    call print_string
    call print_newline
    xor rdi, rdi
    jmp exit
    
    .reading_error:
        mov rdi, error
        call print_string
        mov rdi, 1
        jmp exit
    
    .not_found:
        mov rdi, fail
        call print_string
        mov rdi, 1
        jmp exit
        


