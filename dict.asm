section .text
%include "lib.inc"

global find_word
; Первый аргумент - ключ, второй - указатель на словарь
find_word:
    .cycle:
        push rsi            ; Сохраняем ссылку
    	add rsi, 8	        ; Достаем ключ
    	call string_equals  ; Сравниваем с данным ключом 
    	pop rsi             ; Возвращаем ссылку
    	test rax, rax       ; Проверка на соответствие
    	jne .found          ; Если нашли словов - переходии
    	mov rsi, [rsi]      ; Переходим к следующему
    	test rsi, rsi 	    ; Если дошли до конца
    	je .not_found       ; Выходим
    	jmp .cycle

    .found:
        mov rax, rsi        ; Возвращаем ссылку на эелмент 
        ret
    .not_found:
        xor rax, rax        ; Возвращаем 0
        ret
    

