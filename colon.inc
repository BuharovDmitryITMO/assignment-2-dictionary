%define NEXT_NODE 0

%macro colon 2
    %2:
    dq NEXT_NODE
    db %1, 0
    %define NEXT_NODE %2
%endmacro